import QtQuick 2.4
import "./buttons"
Rectangle{
    id:mainRec
    anchors.fill: parent
    color: "#12aadf"
    radius: 2

    ListModel {
        id:pathmodel
        ListElement {
            name: "Bill Jones"
            icon: "pics/qtlogo.png"
        }
        ListElement {
            name: "Jane Doe"
            icon: "pics/qtlogo.png"
        }
        ListElement {
            name: "John Smith"
            icon: "pics/qtlogo.png"
        }
        ListElement {
            name: "John Smith"
            icon: "pics/qtlogo.png"
        }
        ListElement {
            name: "John Smith"
            icon: "pics/qtlogo.png"
        }
        ListElement {
            name: "John Smith"
            icon: "pics/qtlogo.png"
        }
        ListElement {
            name: "John Smith"
            icon: "pics/qtlogo.png"
        }
        ListElement {
            name: "John Smith"
            icon: "pics/qtlogo.png"
        }
    }


    Component {
        id: delegate
        Rectangle{
            width: 400
            height: 400
            radius: 3
            border.width: 1
            border.color: ma.containsMouse?"#66ffffff":"#11ffffff"
            color: "#33000000"
            z:y
            onXChanged: {
                if(index===0){
                    console.log(x)
                }
            }
            scale: {
                if(x===pathview.width/2-width/2){
                    return 1
                }
                else if(x<(pathview.width/2-width/2)){
                    return (1-0.6*(pathview.width/2-width/2-x)/(pathview.width/2-width/2))
                }

                else{
                    return (1-0.6*(x-pathview.width/2+width/2)/(pathview.width/2-width/2))
                }
            }
            MouseArea{
                id:ma
                anchors.fill: parent
                hoverEnabled: true
                onHoveredChanged: {
                    if(ma.containsMouse){
                        mainWin.itemHoverd=true
                    }else{
                        mainWin.itemHoverd=false
                    }
                }

                onClicked: {
                    loader.source="qrc:/ScenceContent.qml"
                }
                onDoubleClicked: {
                    loader.source="qrc:/ScenceContent.qml"
                }
            }
        }

    }

    PathView {
        id:pathview
        anchors.fill: parent
        model: pathmodel
        delegate: delegate
        path:  Path {
            startX: 0; startY: pathview.height/2-200
            PathCurve { x: pathview.width/2; y: pathview.height/2}
            PathCurve { x: pathview.width; y: pathview.height/2-200 }
        }
    }

    //***********************************test***************************//


    Rectangle{
        width: 50
        height: 50
        color: "#df0000"
        x:mainWin.lefthandx
        y:mainWin.lefthandy
    }
    Rectangle{
        width: 50
        height: 50
        radius: 25
        color: "#aadf12"
        x:mainWin.righthandx
        y:mainWin.righthandy
    }

    //***********************************test***************************//


    //exit bnt
    EXITButton{
        id:exitbnt
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 30
        width: 150
        height: 70
        onMyclicked: {
            mainWin.close()
            Qt.quit()
        }
        onMyhoverdChanged: {
            console.log("item hovered:"+hoverd)
            if(hoverd){
                mainWin.itemHoverd=true
            }
            else{
                mainWin.itemHoverd=false
            }
        }
    }

//    //maxMin bnt
//    MaxMinBnt{
//        id:maxMinBnt
//        width: 150
//        height: 150
//        anchors.horizontalCenter: parent.horizontalCenter
//        onMyclicked: {
//            mainWin.isShownMaximized?mainWin.showNormal():mainWin.showMaximized()
//            isShownMaximized=isShownMaximized?false:true
//        }
//        onMyhoverdChanged: {
//            console.log("item hovered:"+hoverd)
//            if(hoverd){
//                mainWin.itemHoverd=true
//            }
//            else{
//                mainWin.itemHoverd=false
//            }
//        }


//    }

}
