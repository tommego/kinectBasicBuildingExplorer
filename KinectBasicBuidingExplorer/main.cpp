#include <QApplication>
#include <QQmlApplicationEngine>
#include "kinect/kinectsensor.h"
#include <QtQml>
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    qmlRegisterType<KinectSensor>("KinectSensor",1,0,"MyKinectSensor");

    QQmlApplicationEngine engine;
//    QQmlContext context=engine.rootContext();
//    context.setContextProperty("Mouse",engine);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));


    return app.exec();
}
