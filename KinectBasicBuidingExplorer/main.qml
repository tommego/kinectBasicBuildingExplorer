import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import KinectSensor 1.0
import "./widgets"


ApplicationWindow {

    property bool isShownMaximized:false
    property int currentBuildingId:0

    property double lefthandx;
    property double lefthandy;
    property double righthandx;
    property double righthandy;
    property var lefthandState;
    property var righthandState;
    property bool mousePressed:false
    property bool itemHoverd:false

    property int mousePosx;
    property int mousePosy;

    id:mainWin
    width: isShownMaximized?Screen.desktopAvailableWidth:Screen.desktopAvailableWidth*4/5
    height: isShownMaximized?Screen.desktopAvailableHeight:width/2
    visible: true
    x:isShownMaximized?0:Screen.desktopAvailableWidth/2-width/2
    y:isShownMaximized?0:Screen.desktopAvailableHeight/2-height/2
    color: "#00000000"

    Behavior on mousePosx {
        PropertyAnimation{
            properties: "mousePosx"
            duration: 60
        }
    }
    Behavior on mousePosy {
        PropertyAnimation{
            properties: "mousePosy"
            duration: 60
        }
    }


    //initialize somethings
    Component.onCompleted: {
        kinectSensor.setWinPos(mainWin.x,mainWin.y)
        mainWin.showFullScreen()
        isShownMaximized=true
        mousePosx=Screen.width/2
        mousePosy=Screen.height/2
        kinectSensor.refreshMousePos(mousePosx,mousePosy)
        kinectSensor.sendMouseLeftPressEvent()
    }

    //refresh hands positon per time
    Timer {
        id:handPositionGetterTimer
        interval: 60
        running: true
        repeat: true
        onTriggered: {
            kinectSensor.updatebody()

            lefthandState=kinectSensor.getLeftHandState()
            righthandState=kinectSensor.getRightHandState()

            lefthandx=kinectSensor.getLeftHandx()
            lefthandy=kinectSensor.getLeftHandy()
            righthandx=kinectSensor.getRightHandx()
            righthandy=kinectSensor.getRightHandy()

//            new  MouseEvent().x=Math.round(lefthandx)
//            MouseEvent.y=Math.round(lefthandy)

            var gapx=Math.round(kinectSensor.getCurrentMousePositionx()*mainWin.width)
            var gapy=Math.round(mainWin.height-kinectSensor.getCurrentMousePositiony()*mainWin.height)
            if(Math.abs(gapx-mousePosx)>=15){
                mousePosx=gapx
            }
            if(Math.abs(gapy-mousePosy)>=15){
                mousePosy=gapy

            }



            if(lefthandx>0&&lefthandy>0){
//                kinectSensor.refreshMousePos(Math.round(righthandx+mainWin.x+25),Math.round(righthandy+mainWin.y+25))
//                kinectSensor.refreshMousePos(Math.round(kinectSensor.getCurrentMousePositionx()*mainWin.width+mainWin.x+25),Math.round(mainWin.height- kinectSensor.getCurrentMousePositiony()*mainWin.height+mainWin.x+25))
            }

            if(righthandState==="HandState_Closed"){
                if(!mousePressed){
                    kinectSensor.sendMouseLeftPressEvent()
                    mousePressed=true
                }
            }
            else if(righthandState==="HandState_Open"){
                if(mousePressed){
                    kinectSensor.sendMouseLeftReleaseEvent()
                    mousePressed=false
                }
            }
            else{
                if(mousePressed){
                    kinectSensor.sendMouseLeftReleaseEvent()
                    mousePressed=false
                }
            }

            if(mousePressed){
                kinectSensor.sendMouseDragEvent()
            }
        }
    }


    //hover resolve timer
    Timer{
        id:hoverTimer
        interval: 1500
        repeat: true
        onTriggered: {
            kinectSensor.setnMouseLeftClickEvent()
        }
    }
    onItemHoverdChanged: {
        if(itemHoverd===true){
            cursor.isChangedToFull=true
            hoverTimer.start()
        }
        else{
            hoverTimer.stop()
            cursor.isChangedToFull=false
        }
    }

    //kinect sensor ,provide functions whose are tracking bodies datas
    MyKinectSensor{
        id:kinectSensor
    }

    //refresh customized mouse position
    Timer{
        id:mouseRefreshTimer
        interval: 5
        running: true
        repeat: true
        onTriggered: {

            if(kinectSensor.hasTrackingBody()){
                cursor.visible=true
                cursor.x=mousePosx-cursor.width/2
                cursor.y=mousePosy-cursor.width/2
            }else{
                cursor.visible=false
            }
            kinectSensor.refreshMousePos(mousePosx,mousePosy)

        }
    }

    Loader{
        id:loader
        anchors.fill: parent
        source: "qrc:/MainRec.qml"
        visible: true
        focus: true
    }

    //customized cursor
    Cursor{
        id:cursor
        property bool isChangedToFull:false
        Rectangle{
            width: cursor.isChangedToFull?parent.width:0
            height: cursor.isChangedToFull?parent.width:0
            radius: width/2
            color: "#66000000"
//            scale: isChangedToFull?1:0
            anchors.centerIn: parent
            Behavior on width{
                PropertyAnimation{
                    properties: "width"
                    duration: 1500
                }
            }
            Behavior on height{
                PropertyAnimation{
                    properties: "height"
                    duration: 1500
                }
            }
        }
    }

    /******************test***********************/

    Text {
            id: lefthandtext
            text: qsTr("mainwin pos "+mainWin.x+"  ,    "+mainWin.y)
            font.pixelSize: 40
            x:20
            y:20
        }

    Text {
            id: righthandtext
            text: qsTr("cursor position: "+cursor.x+" , "+cursor.y)
            font.pixelSize: 40
            x:20
            y:60
        }
    Text {
            text: qsTr("mouse position: "+mousePosx+" , "+mousePosy)
            font.pixelSize: 40
            x:20
            y:100
        }

    //solve the mouse shape problem

    MouseArea {
            anchors.fill: parent
            cursorShape: Qt.CrossCursor
            acceptedButtons: Qt.NoButton
        }




}
