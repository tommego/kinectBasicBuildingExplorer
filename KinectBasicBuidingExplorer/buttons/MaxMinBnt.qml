import QtQuick 2.0

Rectangle {

    radius: 3
    color: maxMinBntma.containsMouse?"#22000000":"#33000000"
    border.width: 1
    border.color: "#44ffffff"
    antialiasing : true
    smooth: true
    signal myclicked
    signal myhoverdChanged
    property bool hoverd:false
    Behavior on color{
        PropertyAnimation{
            properties: "color"
            duration: 300
        }
    }
    scale: maxMinBntma.containsMouse?0.6:0.5
    Behavior on scale{
        PropertyAnimation{
            properties: "scale"
            duration: 300
            easing.type: Easing.OutBack
        }
    }
    Image {
        id: background
        source: "../imgSource/MaxMinIcon.png"
        anchors.centerIn: parent
    }
    MouseArea{
        anchors.fill: parent
        id:maxMinBntma
        hoverEnabled: true
        onClicked: {
            myclicked()
        }
        onDoubleClicked: {
            myclicked()
        }
        onHoveredChanged: {
            onHoveredChanged: {
                hoverd=maxMinBntma.containsMouse
                myhoverdChanged()
            }
        }
    }
}
