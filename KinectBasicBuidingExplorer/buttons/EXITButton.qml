import QtQuick 2.0

Rectangle {

    radius: 3
    color: exitbntma.containsMouse?"#22000000":"#33000000"
    border.width: 1
    border.color: "#44ffffff"
    antialiasing : true
    smooth: true
    property bool hoverd:false
    signal myclicked
    signal myhoverdChanged
    Behavior on color{
        PropertyAnimation{
            properties: "color"
            duration: 300
        }
    }
    scale: exitbntma.containsMouse?1.2:1
    Behavior on scale{
        PropertyAnimation{
            properties: "scale"
            duration: 300
            easing.type: Easing.OutBack
        }
    }

    Text {
        id: buttext
        text: qsTr("EXIT")
        anchors.centerIn: parent
        color: "#ff8d00"
        font.pixelSize: 40
        antialiasing: true
        smooth: true
    }
    MouseArea{
        anchors.fill: parent
        id:exitbntma
        hoverEnabled: true
        onClicked: {
            myclicked()
        }
        onDoubleClicked: {
            myclicked()
        }
        onHoveredChanged: {
            hoverd=exitbntma.containsMouse
            myhoverdChanged()
        }
    }
}

