import QtQuick 2.0

Rectangle {
    width: 100
    height: 100
    radius: 50
    color: "#44efefef"
    border.width: 1
    border.color: "#333333"
    Behavior on x{
        PropertyAnimation{
            properties: "x"
            duration: 60
        }
    }
    Behavior on y{
        PropertyAnimation{
            properties: "y"
            duration: 60
        }
    }

    Image {
        id: cursorimg
        source: "qrc:/imgSource/cursorimg.png"
    }
}

