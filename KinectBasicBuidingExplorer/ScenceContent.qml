import QtQuick 2.0

Rectangle {


    id:scenceContent
    anchors.fill: parent
    color: "#12aadf"
    radius: 2

    property bool leftHandVisible:false
    property bool rightHandVisible:false

    property int leftPressed_x;
    property int leftPressed_y;
    property int leftDragged_x;
    property int leftDragged_y;

    property int rightPressed_x;
    property int rightPressed_y;
    property int rightDragged_x;
    property int rightDragged_y;


    Path {
        id:lefthandGesture
        startX: leftPressed_x; startY: leftPressed_y
        PathLine { x: leftDragged_x; y: leftDragged_y }
    }
    Path{
        id:righthandGesture
        startX: rightPressed_x; startY: rightPressed_y
        PathLine { x: rightDragged_x; y: rightDragged_y }
    }


    MouseArea{
        id:ma
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onPressed: {
            if(mouse.button==Qt.LeftButton){
                leftPressed_x=mouseX
                leftPressed_y=mouseY
                leftDragged_x=leftPressed_x
                leftDragged_y=leftPressed_y
                leftHandVisible=true
            }
            if(mouse.button==Qt.RightButton){
                rightPressed_x=mouseX
                rightPressed_y=mouseY
                rightDragged_x=rightPressed_x
                rightDragged_y=rightPressed_y
                rightHandVisible=true
            }


        }
        onPositionChanged: {
            if(mouse.buttons==Qt.LeftButton){
                leftDragged_x=mouseX
                leftDragged_y=mouseY
                canvas.paintLefthandeline()
                canvas.requestPaint()
            }
            if(mouse.buttons==Qt.RightButton){
                rightDragged_x=mouseX
                rightDragged_y=mouseY
                canvas.paintRightthandeline()
                canvas.requestPaint()
            }
        }

        onReleased: {
            if(mouse.button==Qt.LeftButton){
                leftHandVisible=false
                canvas.context.clearRect(0, 0, canvas.width, canvas.height)
                canvas.requestPaint()

            }
            if(mouse.button==Qt.RightButton){
                rightHandVisible=false
                canvas.context.clearRect(0, 0, canvas.width, canvas.height)
                canvas.requestPaint()
            }
        }
    }
    Canvas{
        id:canvas
        anchors.fill: parent
        contextType: "2d"
        function paintLefthandeline(){
            canvas.context.clearRect(0, 0, canvas.width, canvas.height)
            context.strokeStyle = Qt.rgba(.2,.2,.2,.5);
            context.lineCap="round"
            context.lineWidth=10
            context.path = lefthandGesture
            context.stroke()
        }
        function paintRightthandeline(){
            canvas.context.clearRect(0, 0, canvas.width, canvas.height)
            context.strokeStyle = Qt.rgba(.2,.2,.2,.5);
            context.lineCap="round"
            context.lineWidth=10
            context.path = righthandGesture
            context.stroke()
        }
    }
    Rectangle{
        id:lefPressedPan
        width: 100
        height: 100
        radius: 50
        x:leftPressed_x-width/2
        y:leftPressed_y-width/2
        visible: leftHandVisible
        color: "#3312aadf"
        border.width: 4
        border.color: "#66333333"
        Rectangle{
            width: (Math.sqrt(((leftDragged_x-leftPressed_x)*
                             (leftDragged_x-leftPressed_x)+
                             (leftDragged_y-leftPressed_y)*
                             (leftDragged_y-leftPressed_y)))+
                   leftDraggedpan.width/2)*2
            height:width
            radius: width/2
            anchors.centerIn: parent
            color: "#11000000"
            visible: parent.visible
        }
    }
    Rectangle{
        id:rightPressedPan
        width: 100
        height: 100
        radius: 50
        visible: rightHandVisible
        color: "#3312aadf"
        x:rightPressed_x-width/2
        y:rightPressed_y-width/2
        border.width: 4
        border.color: "#66333333"
        Rectangle{
            width: (Math.sqrt(((rightDragged_x-rightPressed_x)*
                             (rightDragged_x-rightPressed_x)+
                             (rightDragged_y-rightPressed_y)*
                             (rightDragged_y-rightPressed_y)))+
                   rightDraggedpan.width/2)*2
            height:width
            radius: width/2
            anchors.centerIn: parent
            color: "#11000000"
            visible: parent.visible
        }
    }

    Rectangle{
        id:leftDraggedpan
        width: 50
        height: 50
        color: "#7712aaff"
        radius: 25
        x:leftDragged_x-25
        y:leftDragged_y-25
        visible: leftHandVisible
    }

    Rectangle{
        id:rightDraggedpan
        width: 50
        height: 50
        color: "#7712aaff"
        radius: 25
        x:rightDragged_x-25
        y:rightDragged_y-25
        visible: rightHandVisible
    }
}

