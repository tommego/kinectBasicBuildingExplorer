TEMPLATE = app

QT +=  qml quick widgets 3dcore 3drenderer 3dinput 3dquick core

SOURCES += main.cpp \
    kinect/kinectsensor.cpp \
    window.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

#kinect_V_20 plugin
INCLUDEPATH += $$(KINECTSDK20_DIR)\inc

LIBS += $$(KINECTSDK20_DIR)\Lib\x86\kinect20.lib
#kinect_V_20 plugin


CONFIG += no_lflags_merge

LIBS += "C:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x86\kernel32.lib"
LIBS += "C:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x86\user32.lib"
LIBS += "C:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x86\gdi32.lib"
LIBS += "C:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x86\winspool.lib"
LIBS += "C:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x86\comdlg32.lib"
LIBS += "C:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x86\advapi32.lib"
LIBS += "C:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x86\shell32.lib"
LIBS += "C:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x86\oleaut32.lib"
LIBS += "C:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x86\uuid.lib"
LIBS += "C:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x86\odbc32.lib"
LIBS += "C:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x86\odbccp32.lib"
LIBS += "C:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x86\ole32.lib"
LIBS += "-LC:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x86"

HEADERS += \
    kinect/kinectsensor.h \
    window.h
