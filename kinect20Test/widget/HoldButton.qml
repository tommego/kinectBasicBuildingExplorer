import QtQuick 2.5
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0

Item {
    id:root
    property int bntWidth:120
    property int bntHeight:120
    property string bntText:""
//    property string source:""
    property string bntColor:"#6612aadf"
    property string shadowColor:"#66126688"
    property int textPointSize:20
    property string textColor:"#afafaf"
    signal bntClicked();
    width: bntWidth
    height:bntHeight
    Glow{
        anchors.fill: bntRec
        source:bntRec
        samples: 20
        radius: 20
        color: shadowColor
        scale:bntRec.scale
        transparentBorder: true
        fast: true
        spread: .4
        cached: true
    }

    Rectangle{
        id:bntRec
        anchors.centerIn: parent
        width: parent.width*3/4
        height:width
        color: bntColor
        radius: width/2
        scale: ma.containsMouse?1.2:1
        Behavior on scale{
            PropertyAnimation{
                properties: "scale"
                duration: 150
                easing.type: Easing.OutBack
            }
        }

        Text{
            text:bntText
            anchors.centerIn: parent
            font.pointSize: textPointSize
            color: textColor
            font.bold: true
        }
//        Image {
//            source: root.source
//            anchors.fill: parent
//            fillMode: Image.PreserveAspectCrop
//        }
        MouseArea{
            anchors.fill: parent
            id:ma
            hoverEnabled: true
            onClicked: bntClicked();
        }
    }
}
