import Qt3D 2.0
import Qt3D.Renderer 2.0

Entity
{
    id:rootentity
    property string source;
    components: FrameGraph {
        activeFrameGraph: ForwardRenderer {
            clearColor: Qt.rgba(0.2, 0.2, 0.2, 0.0)
            camera: camera
        }
    }
    Camera {
        id: camera
        projectionType: CameraLens.PerspectiveProjection
        fieldOfView: 45
        aspectRatio: 16/9
        nearPlane : 0.1
        farPlane : 5000.0
        position: Qt.vector3d(camerax,cameray,cameraz )
        upVector: Qt.vector3d( cameraUpVecx,cameraUpVecY,cameraUpVecz)
        viewCenter: Qt.vector3d( cameraCenterx,cameraCentery,cameraCenterz )
    }

//    Configuration  {
//        controlledCamera: camera
//    }

    Entity {
        components : [
            Transform {
                Rotate {angle : root.modelrotationx; axis : Qt.vector3d(1, 0, 0)}
                Rotate {angle : root.modelroatationy; axis : Qt.vector3d(0, 1, 0)}
                Rotate {angle : root.modelrotationz; axis : Qt.vector3d(0, 0, 1)}
                Scale{scale:root.modelscale}
            },
            SceneLoader
            {
//                source: "file:///E:/应用模型/圆柱建筑/03/03.DAE"
                source:mainwin.currentModelPath
            },
            Material {
                parameters: [
                    Parameter {
                        name: "ambient"
                        value: Qt.vector3d(0.0, 0.6,0.0)
                    },
                    Parameter {
                        name: "diffuse"
                        value: Qt.vector3d(0.15, 0.2, 0.2)
                    },
                    Parameter {
                        name: "specular"
                        value: Qt.vector3d(0.9, 0.2, 0.5)
                    }]
            }]
    }
    function resetCamera(){
        camerax=0
        cameray=0
        cameraz=150
        cameraCenterx=0
        cameraCentery=0
        cameraCenterz=0
        cameraUpVectorx=0
        cameraUpVectory=1.0
        cameraUpVectorxz=0
    }
}

