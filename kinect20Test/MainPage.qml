import QtQuick 2.5
import QtQuick.Layouts 1.2
//"file:///E:/应用模型/圆柱建筑/03/03.DAE"
Item {
    id:root
    width:mainwin.width
    height:mainwin.height
    Component.onCompleted: {
        initTimer.start();
        console.log(width,height)
    }

    ListModel{
        id:buildingModel
    }
    Timer{
        id:initTimer
        interval: 400
        property var modelPaths:[
            "file:///D:/buildingModels/豪华会所/a.DAE",
            "file:///D:/buildingModels/积木建筑/a.DAE",
            "file:///D:/buildingModels/设计独特的建筑/a.DAE",
            "file:///D:/buildingModels/世博会国家馆/a.DAE",
            "file:///D:/buildingModels/圆柱建筑/a.DAE"
        ]
        property var imagePaths:[
            "file:///D:/buildingModels/豪华会所/image.png",
            "file:///D:/buildingModels/积木建筑/image.png",
            "file:///D:/buildingModels/设计独特的建筑/image.png",
            "file:///D:/buildingModels/世博会国家馆/image.png",
           "file:///D:/buildingModels/圆柱建筑/image.png"
        ]
        property var label:[
            "豪华会所",
            "积木建筑",
            "设计独特的建筑",
            "世博会国家馆",
            "圆柱建筑"
        ]
        property var currentIndex:0
        repeat: currentIndex<modelPaths.length;
        onTriggered: {
            if(currentIndex<modelPaths.length){
                buildingModel.append({
                                         "modelPath":modelPaths[currentIndex],
                                         "imagePath":imagePaths[currentIndex],
                                         "label":label[currentIndex]
                                     });
                currentIndex++;
            }
        }
    }

    Rectangle{
        id:mainRec
        anchors.fill: parent
        color: "#101010"

        Row{
//            anchors.centerIn: parent
            anchors.verticalCenter: parent.verticalCenter
            x:parent.width/2-width/2
            spacing: 20
            Repeater{
                model: buildingModel
                delegate: Rectangle{
                    width: 0
                    height:0
                    opacity: 0
                    color: "#0012aadf"
                    Behavior on width {
                        PropertyAnimation{
                            properties: "width"
                            duration: 1000
                            easing.type: Easing.OutQuint
                        }
                    }
                    Behavior on height {
                        PropertyAnimation{
                            properties: "height"
                            duration: 1000
                            easing.type: Easing.OutQuint
                        }
                    }
                    Behavior on opacity {
                        PropertyAnimation{
                            properties: "opacity"
                            duration: 1000
//                            easing.type: Easing.OutQuint
                        }
                    }

                    Rectangle{
                        anchors.fill: image
                        scale:1.05
                        color: "#3312aadf"
                        border.width: 1
                        border.color: "#12aadf"
                        radius: 2
                        opacity: ma.containsMouse?1:0
                        Behavior on opacity {
                            PropertyAnimation{
                                properties: "opacity"
                                duration: 200

                            }
                        }
                    }

                    Image {
                        id:image
                        source: imagePath
                        width: parent.width
                        height: width*sourceSize.height/sourceSize.width
                        fillMode: Image.PreserveAspectCrop
                        anchors.centerIn: parent
                        MouseArea{
                            anchors.fill: parent
                            id:ma
                            hoverEnabled: true
                            onClicked: {
                                mainwin.currentModelPath=modelPath;
                                loader.source="qrc:/BigModuelShowPage.qml";
                            }
                        }
                        Text {
                            id: labeltext
                            text: label
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.top: parent.bottom
                            anchors.topMargin: 30
                            color: "#efefef"
                            font.pixelSize: 25
                        }
                    }
                    Component.onCompleted: {
//                        console.log(width,height)
                        width=(root.width-20*6)/5;
                        height= root.height-300;
                        opacity=1;
                    }
                }
            }
        }
    }
}
