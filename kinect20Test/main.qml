import QtQuick 2.5
import QtQuick.Controls 1.2
import QtQuick.Window 2.0
import QtQuick.Layouts 1.2
import KinectSensor 1.0

ApplicationWindow {
    visible: true
    width: Screen.desktopAvailableWidth
    height: Screen.desktopAvailableHeight
    color: "#101010"
    flags: Qt.FramelessWindowHint|Qt.Window
    id:mainwin
    property string currentModelPath:""
//    KinectSensor{
//        id:kinectsensor
//    }

    Component.onCompleted: {
        showMaximized();
    }
    Loader{
        id:loader
        focus: true
        source: "qrc:/MainPage.qml"
        anchors.fill: parent
        width: parent.width
        height: parent.height
    }
}
