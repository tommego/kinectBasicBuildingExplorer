#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <window.h>
#include <Qt3DRenderer/qrenderaspect.h>
#include <Qt3DInput/QInputAspect>
#include <Qt3DQuick/QQmlAspectEngine>

#include <QGuiApplication>
#include <QtQml>
#include "kinect/kinectsensor.h"

int main(int argc, char *argv[])
{
//    QGuiApplication app(argc, argv);

//    QQmlApplicationEngine engine;
//    qmlRegisterType<KinectSensor>("KinectSensors",1,0,"KinectSensor");
//    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

//    return app.exec();
    QGuiApplication app(argc, argv);

//    KinectSensor kinectsensor;

    Window view;
    Qt3D::Quick::QQmlAspectEngine engine;
    qmlRegisterType<KinectSensor>("KinectSensor",1,0,"KinectSensor");

    engine.aspectEngine()->registerAspect(new Qt3D::QRenderAspect());
    engine.aspectEngine()->registerAspect(new Qt3D::QInputAspect());
    QVariantMap data;
    data.insert(QStringLiteral("surface"), QVariant::fromValue(static_cast<QSurface *>(&view)));
    data.insert(QStringLiteral("eventSource"), QVariant::fromValue(&view));
    engine.aspectEngine()->setData(data);
    engine.aspectEngine()->initialize();
    engine.setSource(QUrl("qrc:/main.qml"));
//    engine.setProperty("KinectSensor",&kinectsensor);
//    view.show();
//    view.sh

    return app.exec();
}

