import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Scene3D 2.0
import Qt3D 2.0
import Qt3D.Renderer 2.0
import "./widget" as Widget
import QtQuick.Layouts 1.2
import QtQuick.Controls.Styles 1.4
import KinectSensor 1.0

Item {
    id:root
    property string source;
    focus: true

    //bnt signal
    signal bntCclicked();


    //model properties
    property double modelrotationx:0
    property double modelroatationy:0
    property double modelrotationz:0
    property double modelscale:1

    property int xangle:kinectsensor.leftHandState===2?45:0
    Behavior on xangle{
        PropertyAnimation{
            properties: "xangle"
            duration: 100
        }
    }

    //camera properties
    property double cameraSpeed:cameraSpeedControl.value
    property double cameraCenterx:camerax+Math.sin(cameraRotationy*Math.PI/180)*Math.cos(cameraRotationx*Math.PI/180)*200
    property double cameraCentery:cameray+Math.sin(cameraRotationx*Math.PI/180)*200/**Math.cos(cameraRotationy*Math.PI/180)*/
    property double cameraCenterz:cameraz-Math.cos(cameraRotationy*Math.PI/180)*Math.cos(cameraRotationx*Math.PI/180)*200
    property double cameraRotationy:0
    property double cameraRotationx:0
    property double cameraUpVecx:camerax-Math.sin(cameraRotationx*Math.PI/180)*Math.sin(cameraRotationy*Math.PI/180)*10
    property double cameraUpVecY:cameray+Math.cos(cameraRotationx*Math.PI/180)*100000000
    property double cameraUpVecz:cameraz+Math.sin(cameraRotationx*Math.PI/180)*Math.cos(cameraRotationy*Math.PI/180)*10
    property double camerax:0.0
    property double cameray:20
    property double cameraz:60

    //Scene3D controlling properties
    property bool dragMouse:false
    property bool moveMode:false

    property double clx;
    property double cly;
    property double clz;

    /**
      *states and transitions
      *states for Switching front space ,top space , left space,bottom space coordinate.
    **/
    state: "normal"
    states: [
        State {
            when:!dragMouse
            name: "normal"
            PropertyChanges {
                target: root
            }
        },
                State {
                    when:!dragMouse
                    name: "front"
                    PropertyChanges {
                        target: root
                        cameraRotationx:0;
                        cameraRotationy:0;
                        camerax:0;
                        cameray:0;
                        cameraz:120;
                    }
                },
        State {
            when:!dragMouse
            name: "left"
            PropertyChanges {
                target: root
                cameraRotationx:0;
                cameraRotationy:90;
                camerax:-120;
                cameray:0;
                cameraz:0;

            }
        },
        State {
            when:!dragMouse
            name: "top"
            PropertyChanges {
                target: root
                cameraRotationx:-90;
                cameraRotationy:0;
                camerax:0;
                cameray:120;
                cameraz:0;
            }
        },
        State {
            when:!dragMouse
            name: "bottom"
            PropertyChanges {
                target: root
                cameraRotationx:90;
                cameraRotationy:0;
                camerax:0;
                cameray:-120;
                cameraz:0;
            }
        }
            ]
    transitions: Transition{
        PropertyAnimation{
            target: root
            properties: "camerax,cameray,cameraz,cameraCenterx,cameraCentery,cameraCenterz,cameraRotationy,cameraRotationx"
            duration: 550
            easing.type: Easing.InSine
        }
    }

    //animation for camera rotations
//    Behavior on cameraRotationx{
//        PropertyAnimation{
//            properties: "cameraRotationx"
//            duration: 100
//        }
//    }
//    Behavior on cameraRotationy{
//        PropertyAnimation{
//            properties: "cameraRotationy"
//            duration: 100
//        }
//    }


    /**
      *timers
    **/
        //作手势捕捉失败的时间容差
//    Timer{
//        id:draggingJustifyTimer
//        interval: 50
//        repeat: false
//        onTriggered: {
//            if(kinectsensor.rightHandState!==2){
//                dragMouse=false;
//            }
//            else{
//                dragMouse=true;
//            }
//        }
//    }
    //go back left right camera transtating animation setting
    Timer{
        id:goTimer
        property double vx;
        property double vy;
        property double vz;
        repeat: true
        interval: 10
        onTriggered: {
            vx=cameraSpeed*Math.cos(cameraRotationx*Math.PI/180)*Math.sin(cameraRotationy*Math.PI/180);
//            vy=cameraSpeed*Math.sin(cameraRotationx*Math.PI/180);
            vz=-cameraSpeed*Math.cos(cameraRotationx*Math.PI/180)*Math.cos(cameraRotationy*Math.PI/180);
            camerax+=vx;
//            cameray+=vy;
            cameraz+=vz;
        }
    }
    Timer{
        id:backTimer
        property double vx;
        property double vy;
        property double vz;
        repeat: true
        interval: 10
        onTriggered: {
            vx=-cameraSpeed*Math.cos(cameraRotationx*Math.PI/180)*Math.sin(cameraRotationy*Math.PI/180);
//            vy=-cameraSpeed*Math.sin(cameraRotationx*Math.PI/180);
            vz=cameraSpeed*Math.cos(cameraRotationx*Math.PI/180)*Math.cos(cameraRotationy*Math.PI/180);
            camerax+=vx;
//            cameray+=vy;
            cameraz+=vz;
        }
    }
    Timer{
        id:leftTimer
        property double vx;
        property double vy;
        property double vz;
        repeat: true
        interval: 10
        onTriggered: {
            vx=-cameraSpeed*Math.cos(cameraRotationx*Math.PI/180)*Math.sin((cameraRotationy+90)*Math.PI/180);
//            vy=-cameraSpeed*Math.sin(cameraRotationx*Math.PI/180);
            vz=cameraSpeed*Math.cos(cameraRotationx*Math.PI/180)*Math.cos((cameraRotationy+90)*Math.PI/180);
            camerax+=vx;
//            cameray+=vy;
            cameraz+=vz;
        }
    }
    Timer{
        id:rightTimer
        property double vx;
        property double vy;
        property double vz;
        repeat: true
        interval: 10
        onTriggered: {
            vx=-cameraSpeed*Math.cos(cameraRotationx*Math.PI/180)*Math.sin((cameraRotationy-90)*Math.PI/180);
//            vy=-cameraSpeed*Math.sin(cameraRotationx*Math.PI/180);
            vz=cameraSpeed*Math.cos(cameraRotationx*Math.PI/180)*Math.cos((cameraRotationy-90)*Math.PI/180);
            camerax+=vx;
//            cameray+=vy;
            cameraz+=vz;
        }
    }
    Timer{
        id:upTimer
        repeat: true
        interval: 10
        onTriggered: {
            cameray+=cameraSpeed;
        }
    }
    Timer{
        id:downTimer
        repeat: true
        interval: 10
        onTriggered: {
            cameray-=cameraSpeed;
        }
    }

    /**
      *sensor for controlling
      **/
    //Kienct sensor for controling cursor and Scene3d
    KinectSensor{
        id:kinectsensor
        property int cx;
        property int cy;
        /**0:未知状态
          *1：张开手
          * 2：闭合手
          * 3：剪刀手
          * 4：没有跟踪到
            **/
        onRightHandStateChanged: {
            if(rightHandState===0){
//                draggingJustifyTimer.start();
                dragMouse=false;
            }
            else if(rightHandState===1){
//                draggingJustifyTimer.start();
                dragMouse=false;
            }
            else if(rightHandState===2){
                dragMouse=true;
                cx=cursorx;
                cy=cursory;
            }
            else if(rightHandState===3){
//                draggingJustifyTimer.start();
                dragMouse=false;
            }
            else{
//                draggingJustifyTimer.start();
                dragMouse=false;
            }
        }
        onCursorxChanged: {
            if(dragMouse){
                var dx=cursorx-cx;
                cameraRotationy+=dx/10;
                cx=cursorx;
            }
        }
        onCursoryChanged: {
            if(dragMouse){
                var dy=cursory-cy;
                cameraRotationx-=dy/15;
                cy=cursory;
            }
        }

        onLeftHandStateChanged: {
            if(leftHandState===0){
                moveMode=false;
                goTimer.stop();
                backTimer.stop();
                leftTimer.stop();
                rightTimer.stop();
                upTimer.stop();
                downTimer.stop();
            }
            else if(leftHandState===1){
                moveMode=false;
                goTimer.stop();
                backTimer.stop();
                leftTimer.stop();
                rightTimer.stop();
                upTimer.stop();
                downTimer.stop();
            }
            else if(leftHandState===2){
                if(!moveMode){
                    moveMode=true;
                    clx=leftHandX;
                    cly=leftHandY;
                    clz=leftHandZ;
                }
            }
            else if(leftHandState===3){
                moveMode=false;
                goTimer.stop();
                backTimer.stop();
                leftTimer.stop();
                rightTimer.stop();
                upTimer.stop();
                downTimer.stop();
            }
            else{
                moveMode=false;
                goTimer.stop();
                backTimer.stop();
                leftTimer.stop();
                rightTimer.stop();
                upTimer.stop();
                downTimer.stop();
            }
        }

        onLeftHandXChanged: {
            if((leftHandX-clx>=0.05)&&moveMode){
                if(!rightTimer.running){
                    rightTimer.start();
                }
            }
            else if((leftHandX-clx<=-0.05)&&moveMode){
                if(!leftTimer.running)
                    leftTimer.start();
            }
            else{
                leftTimer.stop();
                rightTimer.stop();
            }
        }

        onLeftHandYChanged: {
            if((leftHandY-cly>=0.05)&&moveMode){
                if(!upTimer.running){
                    upTimer.start();
                }
            }
            else if((leftHandY-cly<=-0.05)&&moveMode){
                if(!downTimer.running){
                    downTimer.start();
                }
            }
            else{
                upTimer.stop();
                downTimer.stop();
            }
        }

        onLeftHandZChanged: {
            if((leftHandZ-clz>=0.05)&&moveMode){
                if(!backTimer.running){
                    backTimer.start();
                }
            }
            else if((leftHandZ-clz<=-0.05)&&moveMode){
                if(!goTimer.running){
                    goTimer.start();
                }
            }
            else{
                goTimer.stop();
                backTimer.stop();
            }
        }
    }

    /**
      *keyboard for camera controlling
      **/
    //keyboard control setting
    Keys.onUpPressed: {
        backTimer.stop();
        leftTimer.stop();
        rightTimer.stop();
        upTimer.stop();
        downTimer.stop();
        goTimer.start();
    }
    Keys.onDownPressed: {
        goTimer.stop();
        leftTimer.stop();
        rightTimer.stop();
        upTimer.stop();
        downTimer.stop();
        backTimer.start();
    }
    Keys.onLeftPressed: {
        goTimer.stop();
        rightTimer.stop();
        backTimer.stop();
        upTimer.stop();
        downTimer.stop();
        leftTimer.start();
    }
    Keys.onRightPressed: {
        goTimer.stop();
        backTimer.stop();
        leftTimer.stop();
        upTimer.stop();
        downTimer.stop();
        rightTimer.start();
    }
    Keys.onDigit8Pressed: {
        goTimer.stop();
        backTimer.stop();
        leftTimer.stop();
        upTimer.start();
        downTimer.stop();
        rightTimer.stop();
    }
    Keys.onDigit2Pressed: {
        goTimer.stop();
        backTimer.stop();
        leftTimer.stop();
        upTimer.stop();
        downTimer.start();
        rightTimer.stop();
    }
    Keys.onReleased: {
        goTimer.stop();
        backTimer.stop();
        leftTimer.stop();
        upTimer.stop();
        downTimer.stop();
        rightTimer.stop();
    }



//    property url source:"file:///E:/"
    Scene3D{
        anchors.fill: parent
        focus: true
        aspects: "input"
        PreviewEntity{
            id:world
        }
    }

    //handle scene rotations
    MouseArea{
        anchors.fill: parent
        id:ma
    }

    //bnts
    Widget.HoldButton{
        id:playbnt
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 30
        bntText: "播放"
        anchors.horizontalCenter: parent.horizontalCenter
        bntWidth: 100
        bntHeight: 100
    }
    RowLayout{
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 30
        anchors.right: parent.right
        anchors.rightMargin: 100
        spacing: 60
        Repeater{
            model: ["正","左","上","下","返回"]
            delegate: Widget.HoldButton{
                bntText: modelData
                width: 100
                height: 100
                onBntClicked: {
                    if(index===0){
                        root.state="front";
                    }
                    else if(index===1){
                        root.state="left";
                    }
                    else if(index===2){
                        root.state="top";
                    }
                    else if(index===3){
                        root.state="bottom";
                    }
                    else if(index===4){
                        loader.source="qrc:/MainPage.qml"
                    }
                }
            }
        }
    }

//    //test and dsplay datas
    ColumnLayout{
        spacing: 30
        x:20
        y:20
        Text{
            text:"cameraPos:"+camerax.toFixed(2)+","+cameray.toFixed(2)+","+cameraz.toFixed(2)
            color: "#12aaef"
        }
        Text{
            text:"cameraCenterPos:"+cameraCenterx.toFixed(2)+","+cameraCentery.toFixed(2)+","+cameraCenterz.toFixed(2)
            color: "#12aaef"
        }
        Text{
            text:"cameraUpPos:"+cameraUpVecx.toFixed(2)+","+cameraUpVecY.toFixed(2)+","+cameraUpVecz.toFixed(2)
            color: "#12aaef"
        }
        Text{
            text:"rotation:"+cameraRotationx.toFixed(2)+","+cameraRotationy.toFixed(2)
            color: "#12aaef"
        }
        /**0:未知状态
          *1：张开手
          * 2：闭合手
          * 3：剪刀手
          * 4：没有跟踪到
            **/
        Text{
            text:kinectsensor.rightHandState===0?"未知状态":
                                                  kinectsensor.rightHandState===1?"张开手":
                                                                                         kinectsensor.rightHandState===2?"闭合手":
                                                                                                                          kinectsensor.rightHandState===3?"剪刀手":"没有跟踪到"
            color: "#12aaef"
        }
        Text{
            text:"cursorx:"+kinectsensor.cursorx.toFixed(2)+"  cursory:"+kinectsensor.cursory.toFixed(2)
            color: "#12aaef"
        }
        Text{
            text:"left hand space x"+kinectsensor.leftHandX.toFixed(2)+"  left hand space y:"+kinectsensor.leftHandY.toFixed(2)+"  left hand space z:"+kinectsensor.leftHandZ.toFixed(2)
            color: "#12aaef"
        }
        Text{
            text:kinectsensor.leftHandState===0?"未知状态":
                                                  kinectsensor.leftHandState===1?"张开手":
                                                                                         kinectsensor.leftHandState===2?"闭合手":
                                                                                                                          kinectsensor.leftHandState===3?"剪刀手":"没有跟踪到"
            color: "#12aaef"
        }

        Text{
            text:"clx:"+clx.toFixed(2)+"  cly:"+cly.toFixed(2)+"  clz:"+clz.toFixed(2)
            color: "#12aaef"
        }
    }

    //camera speed controler
    Slider{
        id:cameraSpeedControl
        value: 0.5
        maximumValue: 20
        minimumValue: 0.3
        orientation: Qt.Horizontal
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 200
        anchors.horizontalCenter: parent.horizontalCenter

        height: 8
        width: parent.width/4
        Text{
            text:"移动速度"
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.left
            anchors.rightMargin: 20
            font.pixelSize: 20
            color: "#12aadf"
            style:Text.Outline
            styleColor: "#66000000"
            font.bold: true
        }

        style: SliderStyle {
            groove: Rectangle {
                width:control.width
                height:control.height
                color: "#12aadf"
                radius: height/2
            }

            handle: Rectangle {
                anchors.centerIn: parent
                color: "#12aadf"
                implicitWidth: 50
                implicitHeight: 50
                radius: width/2
                Text {
                    text: control.value.toFixed(1)
                    anchors.centerIn: parent
                    font.pixelSize: 20
                    color: "#fafafa"
                }
            }
        }
    }

    Rectangle{
        width: 30
        height: width
        radius: height/2
        id:lefthandpos
        color: "#66000000"
        x:kinectsensor.leftHandCoordX
        y:kinectsensor.leftHandCoordY
        Behavior on x{
            PropertyAnimation{
                properties: "x"
                duration: 30
            }
        }
        Behavior on y{
            PropertyAnimation{
                properties: "y"
                duration: 30
            }
        }
        Behavior on width{
            PropertyAnimation{
                properties: "width"
                duration: 100
            }
        }
        transform: Rotation { origin.x: lefthandpos.width/2; origin.y: lefthandpos.height/2; axis { x: 1; y: 0; z: 0 } angle: xangle }
    }

}
